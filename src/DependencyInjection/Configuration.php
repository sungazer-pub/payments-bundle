<?php
/**
 * Created by PhpStorm.
 * User: luca
 * Date: 20/02/19
 * Time: 11.49
 */

namespace Sungazer\Bundle\PaymentsBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

// https://symfony.com/doc/current/bundles/configuration.html

class Configuration implements ConfigurationInterface
{

    /**
     * Generates the configuration tree builder.
     *
     * @return TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('sungazer_payments');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode->children()
            ->scalarNode('stripe_api_key')
            ->defaultValue('%env(resolve:STRIPE_API_KEY)%')->end();

        return $treeBuilder;
    }
}
