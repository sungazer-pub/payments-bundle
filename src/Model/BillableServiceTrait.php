<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;

use Doctrine\ORM\Mapping as ORM;

trait BillableServiceTrait
{
    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $gatewayId;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $type;

    /**
     * @return string
     */
    public function getGatewayId(): string
    {
        return $this->gatewayId;
    }

    /**
     * @param string $gatewayId
     * @return BillableServiceTrait
     */
    public function setGatewayId(string $gatewayId): BillableServiceTrait
    {
        $this->gatewayId = $gatewayId;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return BillableServiceTrait
     */
    public function setName(string $name): BillableServiceTrait
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return BillableServiceTrait
     */
    public function setType(string $type): BillableServiceTrait
    {
        $this->type = $type;
        return $this;
    }
}