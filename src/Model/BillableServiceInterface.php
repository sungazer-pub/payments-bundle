<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface BillableServiceInterface
{
    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getName(): string;
    public function setName(?string $val): self;
}