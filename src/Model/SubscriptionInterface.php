<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface SubscriptionInterface
{
    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getSubscriptionItems();
    public function setSubscriptionItems($val): self;
    public function addSubscriptionItem(SubscriptionItemInterface $item): self;
    public function removeSubscriptionItem(SubscriptionItemInterface $item): self;

    public function getCancelAtPeriodEnd(): bool;
    public function setCancelAtPeriodEnd(bool $val): self;

    public function getCanceled(): bool;
    public function setCanceled(bool $val): self;

    public function getCustomer(): BillableCustomerInterface;
    public function setCustomer(?BillableCustomerInterface $val): self;
}