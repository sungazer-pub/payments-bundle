<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface PlanInterface
{
    const INTERVAL_MONTH = 'month';

    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getPrivateName(): string;
    public function setPrivateName(?string $val): self;

    public function getCurrency(): string;
    public function setCurrency(?string $val): self;

    public function getInterval(): string;
    public function setInterval(?string $val): self;

    public function getUnitPrice(): int;
    public function setUnitPrice(?int $val): self;

    public function getService(): BillableServiceInterface;
    public function setService(?BillableServiceInterface $val): self;
}