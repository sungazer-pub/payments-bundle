<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface SubscriptionItemInterface
{
    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getPlan(): PlanInterface;
    public function setPlan(PlanInterface $val): self;

    public function getQuantity(): int;
    public function setQuantity(int $val): self;

    public function getTaxRates();
    public function setTaxRates($val): self;
    public function addTaxRate(TaxRateInterface $taxRate): self;
    public function removeTaxRate(TaxRateInterface $taxRate): self;
}