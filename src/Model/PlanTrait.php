<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


trait PlanTrait
{

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $gatewayId;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $privateName;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $currency;

    /**
     * @var string
     * @ORM\Column(type="text",nullable=true)
     */
    private $interval;

    /**
     * @var int
     * @ORM\Column(type="int",nullable=true)
     */
    private $unitPrice;
}