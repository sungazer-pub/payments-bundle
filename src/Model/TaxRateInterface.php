<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface TaxRateInterface
{
    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getDisplayName(): string;
    public function setDisplayName(?string $val): self;

    public function getInclusive(): bool;
    public function setInclusive(?bool $val): self;

    public function getJurisdiction(): string;
    public function setJurisdiction(?string $val): self;

    /**
     * Range: 0.0-100.0
     * @return float
     */
    public function getPercentage(): float;

    /**
     * Range: 0.0-100.0
     * @param float|null $val
     * @return $this
     */
    public function setPercentage(?float $val): self;
}