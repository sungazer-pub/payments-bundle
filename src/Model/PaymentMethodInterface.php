<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface PaymentMethodInterface
{
    const TYPE_CARD = 'card';

    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    public function getType(): string;
    public function setType(string $val): self;

    // Card data
    public function getCardHolderName(): ?string;
    public function getCardExpiry(): ?string;
    public function getCardLastDigits(): ?string;
    public function getCardBrand(): ?string;

    public function setCardHolderName(?string $val): self;
    public function setCardExpiry(?string $val): self;
    public function setCardLastDigits(?string $val): self;
    public function setCardBrand(?string $val): self;

}