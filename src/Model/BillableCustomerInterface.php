<?php


namespace Sungazer\Bundle\PaymentsBundle\Model;


interface BillableCustomerInterface
{
    public function getId(): string;

    public function getGatewayId(): ?string;
    public function setGatewayId(?string $val): self;

    /**
     * @return array = [
     *     'address' => [
     *         'line1' => 'string',
     *         'line2' => 'string',
     *         'city' => 'string',
     *         'postal_code' => 'string',
     *         'state' => 'string',
     *         'country' => 'ISO 3166-1 alpha-2',
     *     ],
     *     'description' => 'string',
     *     'email' => 'string',
     *     'name' => 'string',
     *     'phone' => 'string',
     *     'description' => 'string',
     * ]
     */
    public function getGatewayData(): array;

    /**
     * See https://stripe.com/docs/billing/taxes/tax-ids#using-api
     * @return array = [
     *     'type' => 'eu_vat|...',
     *     'value' => 'string'
     * ]
     */
    public function getTaxId(): array;

    public function createPaymentMethod(): PaymentMethodInterface;

}