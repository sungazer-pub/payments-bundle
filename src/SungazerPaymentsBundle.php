<?php

namespace Sungazer\Bundle\PaymentsBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/*
 * Purpose of this bundle is to gather common classes useful to work with payments
 *
 * Author: Luca Nardelli <luca@sungazer.io>
 */

class SungazerPaymentsBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
    }
}

