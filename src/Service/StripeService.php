<?php


namespace Sungazer\Bundle\PaymentsBundle\Service;


use Exception;
use Stripe\Card;
use Stripe\Customer as StripeCustomer;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentMethod;
use Stripe\Plan;
use Stripe\Product;
use Stripe\SetupIntent;
use Stripe\Stripe;
use Stripe\Subscription;
use Stripe\TaxRate;
use Sungazer\Bundle\PaymentsBundle\Model\BillableCustomerInterface;
use Sungazer\Bundle\PaymentsBundle\Model\BillableServiceInterface;
use Sungazer\Bundle\PaymentsBundle\Model\PaymentMethodInterface;
use Sungazer\Bundle\PaymentsBundle\Model\PlanInterface;
use Sungazer\Bundle\PaymentsBundle\Model\SubscriptionInterface;
use Sungazer\Bundle\PaymentsBundle\Model\SubscriptionItemInterface;
use Sungazer\Bundle\PaymentsBundle\Model\TaxRateInterface;

class StripeService
{

    public function __construct($stripeKey)
    {
        Stripe::setApiKey($stripeKey);
    }

    public function syncCustomerToStripe(BillableCustomerInterface $customer)
    {
        if (!$customer->getGatewayId()) {
            $stripeCustomer = StripeCustomer::create($customer->getGatewayData());
            $customer->setGatewayId($stripeCustomer->id);
        } else {
            StripeCustomer::update($customer->getGatewayId(), $customer->getGatewayData());
        }
    }

    public function syncServiceToStripe(BillableServiceInterface $service)
    {
        $payload = [
            'name'     => $service->getName(),
            'type'     => 'service',
            'metadata' => [
                'id' => $service->getId()
            ]
        ];
        if (!$service->getGatewayId()) {
            $stripeEntity = Product::create($payload);
            $service->setGatewayId($stripeEntity->id);
        } else {
            Product::update($service->getGatewayId(), $payload);
        }
    }

    public function syncPlanTostripe(PlanInterface $plan)
    {
        $payload = [
            'nickname'   => $plan->getPrivateName(),
            'usage_type' => 'licensed',
            'currency'   => $plan->getCurrency(),
            'interval'   => $plan->getInterval(),
            'product'    => $plan->getService()->getGatewayId(),
            'amount'     => $plan->getUnitPrice(),
            'metadata'   => [
                'id' => $plan->getId()
            ]
        ];
        if (!$plan->getGatewayId()) {
            $stripeEntity = Plan::create($payload);
            $plan->setGatewayId($stripeEntity->id);
        } else {
            Plan::update($plan->getGatewayId(), $payload);
        }
    }

    public function syncSubscriptionToStripe(SubscriptionInterface $sub)
    {
        $payload = [
            'customer' => $sub->getCustomer(),
            'items'    => [
                'object' => 'list',
                'data'   => array_map(function (SubscriptionItemInterface $item) {
                    return [
                        'plan'      => $item->getPlan()->getGatewayId(),
                        'quantity'  => $item->getQuantity(),
                        'tax_rates' => array_map(function (TaxRateInterface $t) {
                            return $t->getGatewayId();
                        }, $item->getTaxRates()),
                        'metadata'  => [
                            'id' => $item->getId(),
                        ],
                    ];
                }, $sub->getSubscriptionItems())
            ],
            'metadata' => [
                'id' => $sub->getId()
            ]
        ];
        if (!$sub->getGatewayId()) {
            $stripeEntity = Subscription::create($payload);
            $sub->setGatewayId($stripeEntity->id);
        } else {
            Subscription::update($sub->getGatewayId(), $payload);
        }
    }

    public function syncTaxRateToStripe(TaxRateInterface $taxRate)
    {
        $payload = [
            'display_name' => $taxRate->getDisplayName(),
            'inclusive'    => $taxRate->getInclusive(),
            'percentage'   => $taxRate->getPercentage(),
            'jurisdiction' => $taxRate->getJurisdiction(),
            'metadata'     => [
                'id' => $taxRate->getId()
            ]
        ];
        if (!$taxRate->getGatewayId()) {
            $stripeEntity = TaxRate::create($payload);
            $taxRate->setGatewayId($stripeEntity->id);
        } else {
            TaxRate::update($taxRate->getGatewayId(), $payload);
        }

    }

    public function createSetupIntent(array $options = [])
    {
        $options     = array_merge([
            'usage' => 'off_session'
        ], $options);
        $setupIntent = SetupIntent::create($options);
        return $setupIntent;
    }

    /**
     * @param array $data = [
     *     'id' => 'string'
     * ]
     * @param BillableCustomerInterface $targetCustomer
     * @return PaymentMethodInterface
     * @throws ApiErrorException
     */
    public function processConfirmedSetupIntent(string $setupIntentId, BillableCustomerInterface $targetCustomer)
    {
        $stripeSetupIntent = SetupIntent::retrieve($setupIntentId);
        if ($stripeSetupIntent->status !== SetupIntent::STATUS_SUCCEEDED) {
            throw new Exception('unconfirmed_setup_intent');
        }
        $stripePaymentMethod = PaymentMethod::retrieve($stripeSetupIntent->payment_method);
        if ($stripePaymentMethod->type !== 'card') {
            throw new Exception('unsupported_payment_method_type');
        }

        // TODO We should lock here (or we should move this call to another place)
        $this->syncCustomerToStripe($targetCustomer);

        $stripePaymentMethod = $stripePaymentMethod->attach(['customer' => $targetCustomer->getGatewayId()]);
        /** @var Card $stripeCard */
        $stripeCard    = $stripePaymentMethod->card;
        $paymentMethod = $targetCustomer->createPaymentMethod();
        $paymentMethod
            ->setGatewayId($stripePaymentMethod->id)
            ->setCardHolderName($stripePaymentMethod->billing_details['name'])
            ->setCardExpiry(sprintf("%02d/%02d", $stripeCard->exp_month, $stripeCard->exp_year))
            ->setCardLastDigits($stripeCard->last4)
            ->setCardBrand($stripeCard->brand)
            ->setType(PaymentMethodInterface::TYPE_CARD);
        return $paymentMethod;
    }

    /**
     * @param PaymentMethodInterface $paymentMethod
     * @throws ApiErrorException
     */
    public function detachPaymentMethodfromCustomer(PaymentMethodInterface $paymentMethod)
    {
        $res = PaymentMethod::retrieve($paymentMethod->getGatewayId());
        $res->detach();
    }

}